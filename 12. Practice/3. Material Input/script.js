


document.addEventListener('focus', onInputFocus, true)
document.addEventListener('blur', onInputBlur, true)

function onInputFocus(e) {
    console.log('onInputFocus')
    if (e.target.classList.contains('form-input') ) {
        const $continer = e.target.closest('.form-group')

        if ($continer) {
            $continer.classList.add('focused')
        }
    }   
}

function onInputBlur(e) {
    if (e.target.classList.contains('form-input') ) {
        const $continer = e.target.closest('.form-group')

        if ($continer) {
            $continer.classList.remove('focused')
        }
    }   
}


