
document.addEventListener('click', onBtnClick)

function onBtnClick(e) {
    if (e.target.classList.contains('btn') ) {
        const $btn = e.target;
        console.log(e)
        const $circle = document.createElement('span');
        $circle.classList.add('ripple')
        
        $circle.style.top = `${e.offsetY}px`
        $circle.style.left = `${e.offsetX}px`

        $btn.appendChild($circle)
        
        setTimeout(() => {
            $circle.remove()
        }, 500)
    }   
}
