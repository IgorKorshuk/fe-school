

const $total = document.getElementById('total');
const $count = document.getElementById('count');
const $moviePicker = document.getElementById('movie-picker')

document.addEventListener('change', onMoviePickerChange)
document.addEventListener('click', onSeatClick)

populateUI();


let ticketPrice = $moviePicker.value;
let selectedCount = 0;



function onSeatClick(e) {
    if (e.target.classList.contains('seat')) {
        e.target.classList.toggle('seat--selected');
        if (e.target.classList.contains('seat--selected')) {
            selectedCount += 1;
        } else {
            selectedCount -= 1;
        }
        
        updateTotal()
    }
}

function onMoviePickerChange(e) {
    console.log('onMoviePickerChange', e.target.selectedIndex)
    if (e.target.id === 'movie-picker') {
        ticketPrice = +e.target.value
        localStorage.setItem('selectedMovie', e.target.selectedIndex)
        updateTotal()
    }
}

function updateTotal () {
    $count.innerText = selectedCount;
    $total.innerText = selectedCount * ticketPrice;
}

function populateUI() {
    const selectedIndex = localStorage.getItem('selectedMovie')
    if (selectedIndex) {
        $moviePicker.selectedIndex = selectedIndex
    }
}