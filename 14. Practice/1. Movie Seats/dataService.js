
function initDataService() {
    
    const DATA = loadData()

    return {
        saveSeats: saveSeats,
        saveMovie: saveMovie,
        loadSeats: loadSeats,
        loadMovie: loadMovie
    }

    

    function saveSeats(seatsIndexes) {
        console.log('saveSeats', DATA)
        DATA.selectedSeats = seatsIndexes;
        saveData()
    }
    function saveMovie(selectedIndex) {

        DATA.selectedMovie = selectedIndex;
        saveData()
    }

    function loadSeats() {
        return  DATA.selectedSeats
    }

    function loadMovie() {
        return DATA.selectedMovie
    }

    function saveData() {
        localStorage.setItem('DATA', JSON.stringify(DATA) )
    }

    function loadData() {
        return JSON.parse(  localStorage.getItem('DATA') )
    }
}

let DataService = initDataService()

const Foo = 8;
console.log(DataService)