

const $total = document.getElementById('total');
const $count = document.getElementById('count');
const $moviePicker = document.getElementById('movie-picker')
const $seats = document.querySelectorAll('.hall .seat')

document.addEventListener('change', onMoviePickerChange)
document.addEventListener('click', onSeatClick)

populateUI();


let ticketPrice = $moviePicker.value;
let selectedCount = 0;

// SOLID
// S

function onSeatClick(e) {
    if (e.target.classList.contains('seat')) {
        e.target.classList.toggle('seat--selected');
        
        updateSelectedSeats()
        
        updateTotal()
    }
}

function updateSelectedSeats() {
    //  tut
    const $selectedSeats = document.querySelectorAll('.hall .seat--selected')

    const seatsIndexes = [...$selectedSeats].map(function(seat){
        
        return [...$seats].indexOf(seat)
    })
    selectedCount = seatsIndexes.length
    console.log(DataService)
    DataService.saveSeats(seatsIndexes)
}

function onMoviePickerChange(e) {
    console.log('onMoviePickerChange', e.target.selectedIndex)
    if (e.target.id === 'movie-picker') {
        ticketPrice = +e.target.value
        
        DataService.saveMovie(e.target.selectedIndex)
        
        updateTotal()
    }
}

function updateTotal () {
    $count.innerText = selectedCount;
    $total.innerText = selectedCount * ticketPrice;
}

function populateUI() {
    const selectedMovieIndex = DataService.loadMovie();

    if (selectedMovieIndex) {
        $moviePicker.selectedIndex = selectedMovieIndex
    }

    const selectedSeats = DataService.loadSeats()
    console.log('selectedSeats', selectedSeats)
    if (selectedSeats !== null && selectedSeats.length > 0) {
        $seats.forEach(function($seat, index) {
            if (selectedSeats.indexOf(index) > -1) {
                $seat.classList.add('seat--selected')
            }
        })
    }
}