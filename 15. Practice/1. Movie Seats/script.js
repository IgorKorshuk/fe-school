const MOVIES = [
    { price: 10, title: 'Avengers' },
    { price: 12, title: 'Jocker ' },
    { price: 8, title: 'Toy Story 4' },
    { price: 9, title: 'The Lion King' },
];

let selectedMovie = DataService.loadMovie();
let userSeats = DataService.loadSeats();

// even UI (View) events are separeted to View!
// we have eventListeners for abstract data-events, genereted by View
// please try to understend how it is implemented inside view.js
View.addEventListener('seatClick', onSeatClick)
View.addEventListener('moviePickerChange', onMoviePickerChange)

populateUI();

function onSeatClick(newSeat) {
    updateUserSeatData(newSeat);

    DataService.saveSeats(userSeats);

    View.updateView(userSeats, selectedMovie);
}

function onMoviePickerChange(movieIndex) {
    selectedMovie = MOVIES[movieIndex];
    
    DataService.saveMovie(selectedMovie);

    View.updateView(userSeats, selectedMovie);
}

function updateUserSeatData(newSeat) {
    const newSeatIndex = userSeats
            .map( seat => `r${seat.row}p${seat.place}`) // return array of unique strings ex.: ['r1p5', 'r2p8']
            .indexOf(`r${newSeat.row}p${newSeat.place}`) // finding index of unique string generated for newSeat in array of unique strings

    if (newSeatIndex < 0 ) {
        userSeats.push(newSeat);
    } else {
        userSeats.splice(newSeatIndex, 1);
    }
}

function populateUI() {
    const selectedMovieIndex = MOVIES
                                .map(movie => movie.title)
                                .indexOf(selectedMovie && selectedMovie.title); // the same technique to fine indexOf as in updateUserSeatData function
    
    View.setMoviePickerValue(selectedMovieIndex);

    View.updateView(userSeats, selectedMovie);
}
