function initView() {
    const SELECTED_SEAT_CLASS = 'seat--selected';
    const MOVIE_PICKER_ID = 'movie-picker';
    const SEAT_CLASS = 'seat';

    const $total = document.getElementById('total');
    const $count = document.getElementById('count');
    const $seats = document.querySelectorAll(`.hall .${SEAT_CLASS}`)
    const $moviePicker = document.getElementById(MOVIE_PICKER_ID)

    const eventListeners = {
        'seatClick': null,
        'moviePickerChange': null
    }

    document.addEventListener('change', onMoviePickerChange);
    document.addEventListener('click', onSeatClick);
    
    return {
        updateView: updateView,
        setMoviePickerValue: setMoviePickerValue,
        addEventListener: addEventListener,
    }

    function addEventListener(eventName, callback) {
        eventListeners[eventName] = callback
    }

    function onSeatClick(e) {
        if (e.target.classList.contains(SEAT_CLASS)) {
            if (eventListeners.seatClick) {
                const newSeat = {
                    row: e.target.dataset.row,
                    place: e.target.dataset.number,
                };
                
                eventListeners.seatClick(newSeat)
            }
        }
    }

    function onMoviePickerChange(e) {
        if (e.target.id === MOVIE_PICKER_ID) {
            if (eventListeners.moviePickerChange) {
                const movieIndex = +e.target.value;
                
                eventListeners.moviePickerChange(movieIndex)
            }
        }
    }

    function updateSeatsView(seatsData) {
        
        clearSelectedSeatsClass();

        seatsData.forEach((seat) => {
            const $seat = document.querySelector(`[data-number="${seat.place}"][data-row="${seat.row}"]`)

            $seat.classList.add(SELECTED_SEAT_CLASS)            
        })
    }

    function clearSelectedSeatsClass() {
        $seats.forEach(($seat) => {
            $seat.classList.remove(SELECTED_SEAT_CLASS)
        })
    }

    function updateTotal (seatsData, selectedMovie) {
        $count.innerText = seatsData.length;
        $total.innerText = seatsData.length * selectedMovie.price;
    }

    function updateView(seatsData, selectedMovie) {
        updateSeatsView(seatsData)
        updateTotal(seatsData, selectedMovie)
    }

    function setMoviePickerValue(selectedMovieIndex) {
        $moviePicker.selectedIndex = selectedMovieIndex;
    }
}

const View = initView()