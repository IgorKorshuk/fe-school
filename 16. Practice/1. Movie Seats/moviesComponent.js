function initMovieComponent() {
    const MOVIES = [
        { price: 10, title: 'Avengers' },
        { price: 12, title: 'Jocker ' },
        { price: 8, title: 'Toy Story 4' },
        { price: 9, title: 'The Lion King' },
    ];

    const MOVIE_PICKER_ID = 'movie-picker';
    const $moviePicker = document.getElementById(MOVIE_PICKER_ID)

    let data;

    const eventListeners = {
        'movieChanged': null
    }

    document.addEventListener('change', onChange);
    
    render(MOVIES);

    return {
        getData: getData,
        updateView: updateView,
        addUIEventListener: addUIEventListener,
    }

    function render(movies) {
        $moviePicker.innerHTML = createOptions(movies);
    }

    function addUIEventListener(eventName, callback) {
        eventListeners[eventName] = callback
    }

    function createOptions(movies) {
        const options = [];
        movies.forEach( (movie, index) => {
            options.push(`<option value="${index}">${movie.title} ($${movie.price})</option>`)
        })

        return options;
    }

    function updateView(newMovie) {
        data = {...newMovie}
        const selectedMovieIndex = MOVIES
                                .map(movie => movie.title)
                                .indexOf(data && data.title);
        
        $moviePicker.selectedIndex = selectedMovieIndex;
        
    }

    function onChange(e) {
        if (e.target.id === MOVIE_PICKER_ID) {
            const movieIndex = +e.target.value;
            
            data = MOVIES[movieIndex]
            
            if (eventListeners.movieChanged) {
                eventListeners.movieChanged(data)
            }
        }
    }

    function getData() {
        return data;
    }

}