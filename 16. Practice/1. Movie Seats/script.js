const MovieComponent = initMovieComponent();
const SeatsComonent = initSeatsComonent();
const TotalComonent = initTotalComonent();

SeatsComonent.addUIEventListener('seatsChanged', onDataChanged)
MovieComponent.addUIEventListener('movieChanged', onDataChanged)

populateUI();

function onDataChanged() {
    const userSeats = SeatsComonent.getData();
    const selectedMovie = MovieComponent.getData();
    DataService.saveSeats(userSeats);
    DataService.saveMovie(selectedMovie);
    
    TotalComonent.updateView(userSeats, selectedMovie);
}


function populateUI() {
    const selectedMovie = DataService.loadMovie();
    const userSeats = DataService.loadSeats();


    MovieComponent.updateView(selectedMovie)
    SeatsComonent.updateView(userSeats)
    TotalComonent.updateView(userSeats, selectedMovie);    
}
