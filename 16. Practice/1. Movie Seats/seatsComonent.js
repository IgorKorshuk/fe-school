
function initSeatsComonent() { 
    const SELECTED_SEAT_CLASS = 'seat--selected';
    const SEAT_CLASS = 'seat';

    const $seats = document.querySelectorAll(`.hall .${SEAT_CLASS}`);

    let data = [];

    const eventListeners = {
        'seatsChanged': null
    };

    document.addEventListener('click', onSeatClick);

    return {
        getData: getData,
        updateView: updateView,
        addUIEventListener: addUIEventListener
    }

    function addUIEventListener(eventName, callback) {
        eventListeners[eventName] = callback
    }

    function onSeatClick(e) {
        if (e.target.classList.contains(SEAT_CLASS)) {
            
            const newSeat = {
                row: e.target.dataset.row,
                place: e.target.dataset.number,
            };
            
            updateUserSeatData(newSeat)
            
            updateView(data)

            if (eventListeners.seatsChanged) {
                eventListeners.seatsChanged(data)
            }
        }
    }


    function updateUserSeatData(newSeat) {
        const newSeatIndex = data
                .map( seat => `r${seat.row}p${seat.place}`)
                .indexOf(`r${newSeat.row}p${newSeat.place}`)

        if (newSeatIndex < 0 ) {
            data.push(newSeat);
        } else {
            data.splice(newSeatIndex, 1);
        }
    }

    function updateView(seatsData) {
        
        data = [...seatsData]

        clearSelectedSeatsClass();

        data.forEach((seat) => {
            const $seat = document.querySelector(`[data-number="${seat.place}"][data-row="${seat.row}"]`)

            $seat.classList.add(SELECTED_SEAT_CLASS)            
        })
    }

    function clearSelectedSeatsClass() {
        $seats.forEach(($seat) => {
            $seat.classList.remove(SELECTED_SEAT_CLASS)
        })
    }

    function getData() {
        return data;
    }
}