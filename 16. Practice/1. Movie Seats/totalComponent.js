
function initTotalComonent() {
    const $total = document.getElementById('total');
    const $count = document.getElementById('count');

    return {
        updateView: updateView
    }

    function updateView(seatsData, selectedMovie) {
        $count.innerText = seatsData.length;
        $total.innerText = seatsData.length * selectedMovie.price;
    }

}