
function BaseView(foo) {
   
    this.data = null;

    this.updateView = render;
    this.addEventListener = addEventListener;
    this.getValue = getValue,

    function addEventListener(eventName, callback) {
        this.eventListeners[eventName] = callback
    }

    // abstract method
    function render(seatsData, selectedMovie) {
       
    }

    function getValue() {
        return this.data
    }

}

// static method
BaseView.foobar = function() {
    console.log(10)
}