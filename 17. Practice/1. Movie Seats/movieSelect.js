

function MovieSelect() {
    const base  = new BaseView();

    const MOVIES = [
        { price: 10, title: 'Avengers' },
        { price: 12, title: 'Jocker ' },
        { price: 8, title: 'Toy Story 4' },
        { price: 9, title: 'The Lion King' },
    ];
    
    const MOVIE_PICKER_ID = 'movie-picker';
    
    const $moviePicker = document.getElementById(MOVIE_PICKER_ID);

    const eventListeners = {
        'movieChanged': null,
    }
    
    

    document.addEventListener('change', onChange);
    
    render(MOVIES);

    base.foo = 12;
    base.updateView = updateView;

    base.constructor = arguments.callee;

    return base;

    function onChange(e) {
        if (e.target.id === MOVIE_PICKER_ID) {
            const movieIndex = +e.target.value;
            
            this.data  = MOVIES[movieIndex];

            if (eventListeners.movieChanged) {
                eventListeners.movieChanged(this.data )
            }
        }
    }

    function render(movies) {
        const options = createOptions(movies)
        $moviePicker.innerHTML = options.join('')
    }

    function createOptions(movies) {
        const options = [];
        movies.forEach((movie, index) => {
            options.push(`<option value="${index}">${movie.title} ($${movie.price})</option>`)
        })

        return options
    }

    function updateView(newValue) {
        this.data  = newValue;

        const selectedMovieIndex = MOVIES
                                .map(movie => movie.title)
                                .indexOf(this.data  && this.data .title);
        
        $moviePicker.selectedIndex = selectedMovieIndex;
    
    }

}