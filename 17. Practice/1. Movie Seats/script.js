
const View = initView();

// even UI (View) events are separeted to View!
// we have eventListeners for abstract data-events, genereted by View
// please try to understend how it is implemented inside view.js
// View.addEventListener('seatClick', onSeatClick)
View.addEventListener('viewChanged', onViewChanged)
// View.addEventListener('moviePickerChange', onMoviePickerChange)

populateUI();

function onViewChanged(newSeats, newMovie) {
    
    DataService.saveSeats(newSeats);
    DataService.saveMovie(newMovie);
    //View.updateView(userSeats);
}

function populateUI() {
   //  MovieSelect.setValue(selectedMovie)

    // View.setMoviePickerValue(selectedMovieIndex);
    let selectedMovie = DataService.loadMovie();
    let userSeats = DataService.loadSeats();
    
    View.updateView(userSeats, selectedMovie);
}
