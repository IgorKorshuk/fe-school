
function SeatsView() {
    const base  = new BaseView();

    const SELECTED_SEAT_CLASS = 'seat--selected';
    
    const SEAT_CLASS = 'seat';

    const $seats = document.querySelectorAll(`.hall .${SEAT_CLASS}`)
    
    let data = [];

    const eventListeners = {
        'seatsChanged': null,
    }

    document.addEventListener('click', onSeatClick);

    base.updateView = updateView,

    base.constructor = arguments.callee;

    return base;


    function onSeatClick(e) {
        if (e.target.classList.contains(SEAT_CLASS)) {
            
            const newSeat = {
                row: e.target.dataset.row,
                place: e.target.dataset.number,
            };
            
            updateUserSeatData(newSeat);

            if (eventListeners.seatsChanged) {
                eventListeners.seatsChanged(data)
            }
        }
    }

    function updateView(newValue) {
        console.log('updateView', newValue)
        data = [...newValue];

        clearSelectedSeatsClass();

        data.forEach((seat) => {
            console.log('@@@@', seat)
            const $seat = document.querySelector(`[data-number="${seat.place}"][data-row="${seat.row}"]`)

            $seat.classList.add(SELECTED_SEAT_CLASS)            
        })
    }

    function getValue() {
        return userSeats;
    }

    function clearSelectedSeatsClass() {
        $seats.forEach(($seat) => {
            $seat.classList.remove(SELECTED_SEAT_CLASS)
        })
    }
    
    function updateUserSeatData(newSeat) {
        const newSeatIndex = data
                .map( seat => `r${seat.row}p${seat.place}`) // return array of unique strings ex.: ['r1p5', 'r2p8']
                .indexOf(`r${newSeat.row}p${newSeat.place}`) // finding index of unique string generated for newSeat in array of unique strings

        if (newSeatIndex < 0 ) {
            data.push(newSeat);
        } else {
            data.splice(newSeatIndex, 1);
        }
    }
}
