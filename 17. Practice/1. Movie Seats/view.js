function initView() {
    const eventListeners = {
        'viewChanged': null
        // 'moviePickerChange': null
    }

    const totalView = new TotalView(9);
    const movieSelect = new MovieSelect();
   
    const seatsView = new SeatsView();

    const views = [totalView, totalView, seatsViews]

    console.log(movieSelect)
    window.movieSelect = movieSelect

    movieSelect.addEventListener('movieChanged', onMoviePickerChange);
    seatsView.addEventListener('seatsChanged', onSeatsChange);
    
    return {
        updateView: updateView,
        addEventListener: addEventListener,
    }

    function addEventListener(eventName, callback) {
        eventListeners[eventName] = callback
    }

    function onSeatsChange(newSeats) {
        updateView(seatsView.getValue(), movieSelect.getValue())
        eventListeners.viewChanged(seatsView.getValue(), movieSelect.getValue())
    }
    function onMoviePickerChange(newMovie) {
        updateView(seatsView.getValue(), movieSelect.getValue())
        eventListeners.viewChanged(seatsView.getValue(), seatsView.getValue())
    }

    function updateView(seatsData, selectedMovie) {
        views.forEach((view) => {
            view.updateView(seatsData, selectedMovie)
        })


        // seatsView.updateView(seatsData)
        // movieSelect.updateView(selectedMovie)
        // totalView.updateView(seatsData, selectedMovie)
        // updateTotal(seatsData, selectedMovie)
    }
}