document.getElementById('salary').innerHTML =
    '<div id="rates"></div><div id="reload"></div>';

function loadRates() {
    fetch('data/rates.json')
        .then((response) => response.json())
        .then((data) => {
            var d = new Date();
            var hrs = d.getHours();
            var mins = d.getMinutes();
            var sec = d.getSeconds();

            var msg = '<h3>Средняя зарплата</h3>';

            for (var key in data) {
              msg += '<div class="' + key + '">' + key + ': ' + data[key] + '</div>';
            }

            msg += '<br>Last update: ' + hrs + ':' + mins + ':' + sec + '<br>';

            document.getElementById('rates').innerHTML = msg;
        })
        .catch(function (err) {
            console.log(err)
            document.getElementById('rates').innerText =
                'Всё сломалось ничего не работает!';
        })
        .finally(function () {
            var reload = '<a id="refresh" href="#">';

            reload += '<img src="img/refresh.png" alt="refresh" /></a>';

            document.getElementById('reload').innerHTML = reload;

            document.getElementById('refresh').addEventListener('click', function (e) {
                e.preventDefault();

                loadRates();
            });
        });
}

loadRates();
