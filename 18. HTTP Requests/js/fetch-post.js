const $form = document.getElementById('register');

$form.addEventListener('submit', function (e) {
    e.preventDefault();

    fetch('http://localhost/register.php', {
        method: 'POST',
        body: new FormData(e.target),
    })
        .then((response) => {
            if (response.ok) {
                return response.text();
            }
            return Promise.reject(response);
        })
        .then((data) => {
            document.getElementById('register').innerHTML = data;
        })
        .catch(function (error) {
            console.warn(error);
        });
});
