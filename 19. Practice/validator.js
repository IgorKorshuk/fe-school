const VALIDATOR = {
    'requiered': (value) => !!(value.length === 0),
    'min-length':  (value, param) => !!(value.length < param),
    'max-length':  (value, param) => !!(value.length > param),
    'confirm-password': (value, param) => !!(value === document.querySelector(param).value),
}