
(async function start() {
    const $movieList = document.getElementById('main')
    const $movieTemplate = document.getElementById('movieTemplate')
    const movies = await DataService.getMovies()
    let moviesHTML = '';
    console.log(movies)
    
    movies.forEach(drawMovieCard);

    $movieList.innerHTML = moviesHTML


    function drawMovieCard(movie) {
        movie.imageSrc = DataService.getPicturePath(movie.poster_path);
        
        let template =  $movieTemplate.innerHTML;

        const matches = template.match(/\{\{.+?\}\}/g);
        //{{title}}
        matches.forEach(match => {
            const name = match.replace('{{', '').replace('}}', '')
            console.log(match, name)
            template = template.replace(match, movie[name])
        })

        moviesHTML += template
    }

})()