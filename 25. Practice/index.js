const http = require('http');
const fs = require('fs');

const server = http.createServer(requestHandler)


function requestHandler(req, res) {
    console.log(req.body)
    if (req.url === '/form' && req.method === 'POST') { 
        res.end('<h1>Form submited</h1>')
    }
    else if (req.url.indexOf('/css/') > -1) {
        const filename = req.url.split('/css/')[1];
        console.log(filename)
        
        fs.readFile('./public/' + filename, function(err, file){
            res.end(file)
        })
        
    }
    else if (req.url.indexOf('/user') > -1) {
        res.end(`<h1>Hello World, ${req.url.split('/user/')[1]}</h1>`)
    } else {
        res.end('<html><head><title>My page</title><link rel="stylesheet" href="/css/main.css" /></head><body><h1>Hello World, Sergey</h1></body></html>')
    } 
   
}


server.listen(4000, (err) => {
    if (err) {
        return console.log('Error', err)
    }
    console.log('server started')
})

