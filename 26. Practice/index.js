const path = require('path')
const express = require('express');
const { response } = require('express');

const app = express();
const publicPath = path.join( __dirname , './public' )

const staticMiddleware = express.static(publicPath)

app.use(staticMiddleware)

app.use(userMiddleWare)
app.use(cansoleMiddleWare)


app.get('/', [userMiddleWare, cansoleMiddleWare], (req, res) => {

    const HelloWorld = 'Hello World';
    res.send(`<html><head><title>My page</title><link rel="stylesheet" href="/css/main.css" /></head><body><h1>${HelloWorld}, Sergey ${res.locals.x}</h1></body></html>`)
})
app.get('/specialpage.html', (req, res) => {
    res.send('<html><head><title>My page</title><link rel="stylesheet" href="/css/main.css" /></head><body><h1>specialpage</h1></body></html>')
})
app.get('/:page.html', (req, res) => {
    const pageName = req.params.page
    res.send('<html><head><title>My page</title><link rel="stylesheet" href="/css/main.css" /></head><body><h1>Hello World, ' + pageName + '</h1></body></html>')
})
app.post('/form', (req, res) => {
    res.end('<h1>Form submited</h1>')
})

app.get('*', (req, res) => {
    res.end('<h1>404</h1>')
})

app.listen(4000, (err) => {
    if (err) {
        return console.log('Error', err)
    }
    console.log('server started')
})

function cansoleMiddleWare(req, res, next) {
    console.log('###### cansoleMiddleWare')
    next()
}

function userMiddleWare(req, res, next) {
    console.log('###### userMiddleWares')
    const x = Math.random();
    // res.x = x;
    // req.x = x;
    res.locals.x = x;
    
    next()
}


// const http = require('http');
// const fs = require('fs');

// const server = http.createServer(requestHandler)


// function requestHandler(req, res) {
//     console.log(req.body)
//     if (req.url === '/form' && req.method === 'POST') { 
//         res.end('<h1>Form submited</h1>')
//     }
//     else if (req.url.indexOf('/css/') > -1) {
//         const filename = req.url.split('/css/')[1];
//         console.log(filename)
        
//         fs.readFile('./public/' + filename, function(err, file){
//             res.end(file)
//         })
        
//     }
//     else if (req.url.indexOf('/user') > -1) {
//         res.end(`<h1>Hello World, ${req.url.split('/user/')[1]}</h1>`)
//     } else {
//         array // [{}]
//         var html = '<html><head><title>My page</title><link rel="stylesheet" href="/css/main.css" /></head><body>';

//         for obj in array 
//             var objHtml = f(obj)
//             html += objHtml
        
//             html+=    '</body></html>'

//         res.end(html)
//     } 
   
// }


// server.listen(4000, (err) => {
//     if (err) {
//         return console.log('Error', err)
//     }
//     console.log('server started')
// })

