const $counters = document.querySelectorAll('.counter')

$counters.forEach(startCounter);


function startCounter($counter) {
    const target = $counter.dataset.target;
    const step = target / 200;
    let now = 0;
    
    updateCounter($counter, now, target, step);
}

function updateCounter($counter, now, target, step) {
    if (now < target) {
        now += step;
        $counter.textContent = Math.ceil(now);
        setTimeout(function(){
            updateCounter($counter, now, target, step)
        }, 1)
    }
}

