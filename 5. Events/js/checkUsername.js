var elMsg = document.getElementById('feedback');
var elUsername = document.getElementById('username');
var elPassword = document.getElementById('password');

document.addEventListener('click', checkUsername); 

document.addEventListener('DOMContentLoaded', setup, false);

function checkUsername(event) {
  var $target = event.target;

  if ($target.className.indexOf('input') > -1) {
    var count = +elUsername.dataset.symbols;
    if (elUsername.value.length < count) {
      elMsg.textContent = 'Username must be ' + count + ' characters or more';
    } else {
      elMsg.textContent = '';
    }
  }

}

function setup() {
  elUsername.focus();
}

function logClick() {
  console.log('click')
}

	
var msg = '<div class="header"><a id="close" href="#">close X</a></div>';
msg += '<div><h2>System Maintenance</h2>';
msg += 'Our servers are being updated between 3 and 4 a.m. ';
msg += 'During this time, there may be minor disruptions to service.</div>';
 
var elNote = document.createElement('div');
elNote.setAttribute('id', 'note');
elNote.innerHTML = msg;
document.body.appendChild(elNote);
 
function dismissNote() {
  document.body.removeChild(elNote);
}
 
var elClose = document.getElementById('close');
elClose.addEventListener('click', dismissNote, false);


window.addEventListener('beforeunload', function(event) {
  var message = 'You have changes that have not been saved';
  (event || window.event).returnValue = message;
  return message;
});