function itemDone(e) {
  e.preventDefault();

  var target, elParent, elGrandparent;

  target = e.target;
  elParent = target.parentNode;
  elGrandparent = target.parentNode.parentNode;
  elGrandparent.removeChild(elParent);
}

var el = document.getElementById('list');

el.addEventListener('click', function(e) {
  itemDone(e);
}, false);
