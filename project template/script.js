
const $total = document.getElementById('total');
const $count = document.getElementById('count');

document.addEventListener('click', onMoviePickerChange, true)

let ticketPrice = 0;

function onMoviePickerChange(e) {
    console.log('onMoviePickerChange', e)
    if (e.target.id === 'movie-picker') {
        ticketPrice = +e.target.value

        updateTotal()
    }
}

function updateTotal () {
    const selectedCount = 3;

    $count.innerText = selectedCount;
    $total.innerText = selectedCount * ticketPrice;
}